<?php include ("db.php") ?>
<?php include ("includes/header.php") ?>
<?php include ("includes/footer.php") ?>


<div class="container">

    <div class="form">
        <?php if(isset($_SESSION['msg'])) { ?>
<div class="center">
            <p id="x" class="alert <?= $_SESSION['color']  ?> "> <?= $_SESSION['msg'] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;X    </p>


</div>


        <?php session_unset(); } ?>
        <form class="register-form" action="save_task.php" method="POST">
            <input type="text" name="title" placeholder="Task title"/>
            <textarea name="description" placeholder="Description"/></textarea>
            <button class="green" name="save_task" type="submit" value="save task">create</button>
        </form>
    </div>



    <div class="list">
       <table>
           <thead class="thead">
           <tr>
               <th>
                   Title
               </th>
               <th>
                   Description
               </th>
               <th>
                   Create at
               </th>
               <th>
                   More
               </th>
           </tr>
           </thead>
           <tbody>
       <?php
       $query = "SELECT * FROM task";
       $result = mysqli_query($conn, $query );

       while ($row = mysqli_fetch_array($result)) { ?>
           <tr>
               <td> <?php  echo $row['title'] ?></td>
               <td> <?php  echo $row['description'] ?></td>
               <td> <?php  echo $row['date'] ?></td>
               <td> <a class="links green" href="edit_task.php?id=<?php echo $row['id'] ?>" > Edit </a>
                   <a  class="links red" href="delete_task.php?id=<?php echo $row['id'] ?>" > Delete </a>

               </td>





           </tr>



       <?php } ?>
           </tbody>
       </table>
    </div>

</div>



